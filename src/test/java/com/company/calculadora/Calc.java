package com.company.calculadora;

import java.util.ArrayList;
import java.util.List;

public class Calc {

	public double calcular(String exp) {

		List<Double> numeros = new ArrayList<Double>();
		List<Character> operadores = new ArrayList<Character>();

		numeros = this.getNumbers(exp);
		operadores = this.getOperators(exp);

		double total = 0;

		operadores.add(0, '+');
		// numeros.add(0, total);

		System.out.println(numeros);
		System.out.println(operadores);

		for (int i = 0; i < operadores.size(); i++) {

			double num1 = total;
			double num2 = numeros.get(i);
			char op = operadores.get(i);
			if (isSumOrSubsctraction(op)) {
				op = '+';
			}

			total = executarOperacao(total, op, num2);

			System.out.println(num1 + "  " + op + num2 + "=" + total);
		}

		return total;

		// int signalIdx = -1;
		//
		// if ((signalIdx = exp.indexOf("+", 1)) > 0) {
		// double num1 = Float.parseFloat(exp.substring(0, signalIdx));
		// double num2 = Float.parseFloat(exp.substring(signalIdx + 1));
		//
		// return num1 + num2;
		//
		// } else if ((signalIdx = exp.indexOf("-", 1)) > 0) {
		//
		// double num1 = Float.parseFloat(exp.substring(0, signalIdx));
		// double num2 = Float.parseFloat(exp.substring(signalIdx + 1));
		//
		// return num1 - num2;
		//
		// } else if ((signalIdx = exp.indexOf("/", 1)) > 0) {
		//
		// double num1 = Float.parseFloat(exp.substring(0, signalIdx));
		// double num2 = Float.parseFloat(exp.substring(signalIdx + 1));
		//
		// return num1 / num2;
		// }
		//
		// return -1;
	}

	private List<Character> getOperators(String exp) {
		List<Character> operadores = new ArrayList<Character>();

		for (int i = 0; i < exp.length(); i++) {

			char c = exp.charAt(i);

			switch (c) {
				case '+':
				case '-':
				case '/':
				case '*':
					if (i != 0)
						operadores.add(new Character(c));
					break;
			}

		}
		return operadores;
	}

	private List<Double> getNumbers(String exp) {
		List<Double> numbers = new ArrayList<Double>();

		// Quando termina um numero ?
		// 1. quando antes é um operador, e não é o inicio
		// 2. quando está no final da expressao

		String numberStr = "";
		for (int i = 0; i < exp.length(); i++) {

			char c = exp.charAt(i);

			if (this.isOperator(c)) {

				if (numberStr.isEmpty()) {
					numberStr = numberStr.concat("" + c);
				} else {
					// System.out.println(c);
					numbers.add(Double.parseDouble(numberStr));
					numberStr = "" + (isDivideOrMultiplier(c) ? "" : c);
				}
			} else if (i == exp.length() - 1) {
				numberStr = numberStr.concat("" + c);

				// System.out.println(c);
				numbers.add(Double.parseDouble(numberStr));
			} else {
				numberStr = numberStr.concat("" + c);
			}

		}

		return numbers;
	}

	private double executarOperacao(double numero1, char operador, double numero2) {
		double resultado = 0.0;

		if (operador == '+') {
			resultado = numero1 + numero2;
		} else if (operador == '-') {
			resultado = numero1 - numero2;
		} else if (operador == '/') {
			resultado = numero1 / numero2;
		} else if (operador == '*') {
			resultado = numero1 * numero2;
		}
		return resultado;
	}

	private boolean isOperator(Character c) {

		return this.isDivideOrMultiplier(c) || this.isSumOrSubsctraction(c);
	}

	private boolean isDivideOrMultiplier(Character c) {

		switch (c) {
			case '/':
			case '*':
				return true;
			default:
				return false;
		}
	}

	private boolean isSumOrSubsctraction(Character c) {

		switch (c) {
			case '+':
			case '-':
				return true;
			default:
				return false;
		}
	}

}
