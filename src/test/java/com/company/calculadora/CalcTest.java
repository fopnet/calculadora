package com.company.calculadora;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalcTest {
	
	@Test
	public void testSum1MaisUm() {
		
		Calc c = new Calc();
		
		double resultado = c.calcular("1+1");
		
		assertEquals(2, resultado, 0);
		
	}
	
	@Test
	public void testSum1MaisDois() {
		
		Calc c = new Calc();
		
		double resultado = c.calcular("1+2");
		
		assertEquals(3.0f, resultado, 0);
		
	}
	
	@Test
	public void testSubCincoMenosUm() {
		
		Calc c = new Calc();
		
		double resultado = c.calcular("5-1");
		
		assertEquals(4, resultado, 0);
		
	}
	
	@Test
	public void testSubUmDivideDois() {
		
		Calc c = new Calc();
		
		double resultado = c.calcular("1/2");
		
		assertEquals(.5f, resultado, 0);
		
	}
	
	@Test
	public void testMenosMeioMenos3() {
		
		Calc c = new Calc();
		
		double resultado = c.calcular("-0.5-3");
		
		assertEquals(-3.5f, resultado, 0);
		
	}
	
	@Test
	public void testMenosUmMaisDozeMenosQuatro() {
		
		Calc c = new Calc();
		
		double resultado = c.calcular("-1+12-4");
		
		assertEquals(7, resultado, 0);
		
	}

}
